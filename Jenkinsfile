#!groovy

import com.cloudbees.groovy.cps.NonCPS
import java.io.File
import java.util.UUID
import groovy.json.JsonOutput;
import groovy.json.JsonSlurperClassic;

  def image = 'icppocjavaspring'
  def maven = 'maven:3.5.2-jdk-8'
  def docker = 'ibmcom/docker:17.10'
  def kubectl = 'ibmcom/k8s-kubectl:v1.8.3'
  def helm = 'lachlanevenson/k8s-helm:v2.7.2'
  def mvnCommands = 'package'
  def registry = 'mycluster.icp:8500/microclimate/'
  def registrySecret = 'pipeline-registry-secret'
  def serviceAccountName = 'devops-pipeline-sa'
  def noGitSslVerify = true

  def helmSecret = 'pipeline-helm-secret'
  def chartFolder = 'chart'
  def alwaysPullImage = false
  def helmTlsOptions = " --tls --tls-ca-cert=/pipeline_helm_sec/ca.pem --tls-cert=/pipeline_helm_sec/cert.pem --tls-key=/pipeline_helm_sec/key.pem " 

  print "DevOpsPipeline: registry=${registry} registrySecret=${registrySecret} \
  chartFolder=${chartFolder} alwaysPullImage=${alwaysPullImage} serviceAccountName=${serviceAccountName} noGitSslVerify=${noGitSslVerify}"


  def jobName = (env.JOB_BASE_NAME)
  // E.g. JOB_NAME=myproject/master
  def jobNameSplit = env.JOB_NAME.split("/")
  def projectName = jobNameSplit[0]
  def branchName = jobNameSplit[1]

  // We won't be able to get hold of registrySecret if Jenkins is running in a non-default namespace that is not the deployment namespace.
  // In that case we'll need the registrySecret to have been ported over, perhaps during pipeline install.

  // Only mount registry secret if it's present
  def volumes = [ hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock') ]
  volumes += hostPathVolume(hostPath: '/root/.m2/', mountPath: '/root/.m2/')
  if (registrySecret) {
    volumes += secretVolume(secretName: registrySecret, mountPath: '/pipeline_reg_sec')
  }
  if (helmSecret) {
    volumes += secretVolume(secretName: helmSecret, mountPath: '/pipeline_helm_sec')
  }
  print "DevOpsPipeline: volumes = ${volumes}"
  print "DevOpsPipeline: helmSecret: ${helmSecret}"

  podTemplate(
    cloud: 'devops-pipeline',
    label: 'devopsPipelinePod',
    inheritFrom: 'devops-pipeline',
    serviceAccount: serviceAccountName,
    containers: [
      containerTemplate(name: 'maven', image: maven, ttyEnabled: true, command: 'cat'),
      containerTemplate(name: 'docker', image: docker, command: 'cat', ttyEnabled: true,
        envVars: [
          containerEnvVar(key: 'DOCKER_API_VERSION', value: '1.23.0')
        ]),
      containerTemplate(name: 'kubectl', image: kubectl, ttyEnabled: true, command: 'cat'),
      containerTemplate(name: 'helm', image: helm, ttyEnabled: true, command: 'cat'),
    ],
    volumes: volumes
  ) {
    node('devopsPipelinePod') {
      def gitCommit
      def previousCommit
      def gitCommitMessage
      def fullCommitID

      if (noGitSslVerify) {
		echo "skip SSL verification for git commands."
		sh(script: 'git config --global http.sslVerify false')
      }

      stage ('Extract') {
	  checkout scm
	  fullCommitID = sh(script: 'git rev-parse HEAD', returnStdout: true).trim()
	  gitCommit = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
	  previousCommitStatus = sh(script: 'git rev-parse -q --short HEAD~1', returnStatus: true)      
	  // If no previous commit is found, below commands need not run but build should continue
	  // Only run when a previous commit exists to avoid pipeline fail on exit code
	  if (previousCommitStatus == 0){ 
	    previousCommit = sh(script: 'git rev-parse -q --short HEAD~1', returnStdout: true).trim()
	    echo "Previous commit exists: ${previousCommit}"
	  }
	  gitCommitMessage = sh(script: 'git log --format=%B -n 1 ${gitCommit}', returnStdout: true)
	  echo "Checked out git commit ${gitCommit}"
      }

      def imageTag = null
      def helmInitialized = false // Lazily initialize Helm but only once
      if (build) {
        if (fileExists('pom.xml')) {
          stage ('Maven Build') {
            container ('maven') {
              def mvnCommand = "mvn -B"
              mvnCommand += " ${mvnCommands}"
              sh mvnCommand
            }
          }
        }

        if (fileExists('Dockerfile')) {
          
          stage ('Docker Build') {
            container ('docker') {
              imageTag = gitCommit
              def buildCommand = "docker build -t ${image}:${imageTag} "
              buildCommand += "--label org.label-schema.schema-version=\"1.0\" "
              def scmUrl = scm.getUserRemoteConfigs()[0].getUrl()
              buildCommand += "--label org.label-schema.vcs-url=\"${scmUrl}\" "
              buildCommand += "--label org.label-schema.vcs-ref=\"${gitCommit}\" "  
              buildCommand += "--label org.label-schema.name=\"${image}\" "
              def buildDate = sh(returnStdout: true, script: "date -Iseconds").trim()
              buildCommand += "--label org.label-schema.build-date=\"${buildDate}\" "
              if (alwaysPullImage) {
                buildCommand += " --pull=true"
              }
              if (previousCommit) {
                buildCommand += " --cache-from ${registry}${image}:${previousCommit}"
              }
              
              buildCommand += " ."
              if (registrySecret) {
                sh "ln -s /pipeline_reg_sec/.dockercfg /home/jenkins/.dockercfg"
                sh "mkdir /home/jenkins/.docker"
                sh "ln -s /pipeline_reg_sec/.dockerconfigjson /home/jenkins/.docker/config.json"
              }
              sh buildCommand
              if (registry) {
                sh "docker tag ${image}:${imageTag} ${registry}${image}:${imageTag}"
                sh "docker push ${registry}${image}:${imageTag}"
              }
            }
          }
        }
      }

      def realChartFolder = null
      if (fileExists(chartFolder)) {
        // find the likely chartFolder location
        realChartFolder = chartFolder + '/' + projectName 
        def yamlContent = "image:"
        yamlContent += "\n  repository: ${registry}${image}"
        if (imageTag) yamlContent += "\n  tag: \\\"${imageTag}\\\""
        sh "echo \"${yamlContent}\" > pipeline.yaml"
      }

      if (fileExists('pom.xml') && realChartFolder != null && fileExists(realChartFolder)) {
        stage ('Verify') {
          testNamespace = "testns-${env.BUILD_ID}-" + UUID.randomUUID()
          print "testing against namespace " + testNamespace
          String tempHelmRelease = (image + "-" + testNamespace)
          // Name cannot end in '-' or be longer than 53 chars
          while (tempHelmRelease.endsWith('-') || tempHelmRelease.length() > 53) tempHelmRelease = tempHelmRelease.substring(0,tempHelmRelease.length()-1)
          container ('kubectl') {
            sh "kubectl create namespace ${testNamespace}"
            sh "kubectl label namespace ${testNamespace} test=true"
            if (registrySecret) {
              giveRegistryAccessToNamespace (testNamespace, registrySecret)
            }
          }

          if (!helmInitialized) {
            initalizeHelm ()
            helmInitialized = true
          }
          deployProject (realChartFolder, registry, image, imageTag, testNamespace, registrySecret, helmSecret, helmTlsOptions)
          
        }
      }

      def result="commitID=${gitCommit}\\n" + 
           "fullCommit=${fullCommitID}\\n" +
           "commitMessage=${gitCommitMessage}\\n" + 
           "registry=${registry}\\n" + 
           "image=${image}\\n" + 
           "imageTag=${imageTag}"
      
      sh "echo '${result}' > buildData.txt"
      archiveArtifacts 'buildData.txt'

    }
  }



def initalizeHelm () {
  container ('helm') {
    sh "helm init --skip-refresh --client-only"     
  }
}

def deployProject (String chartFolder, String registry, String image, String imageTag, String namespace, String registrySecret, String helmSecret, String helmTlsOptions) {
  if (chartFolder != null && fileExists(chartFolder)) {
    container ('helm') {
      def deployCommand = "helm upgrade --install --wait --values pipeline.yaml"
      if (fileExists("chart/overrides.yaml")) {
        deployCommand += " --values chart/overrides.yaml"
      }
      if (namespace) {
        deployCommand += " --namespace ${namespace}"
        createNamespace(namespace, registrySecret)   
      }
      if (helmSecret) {
        echo "adding --tls"
        deployCommand += helmTlsOptions
      }
      def releaseName = (env.BRANCH_NAME == "master") ? "${image}" : "${image}-${env.BRANCH_NAME}"
      deployCommand += " ${releaseName} ${chartFolder}"
      sh deployCommand
    }
  }
}



/*
  Create target namespace and give access to regsitry
*/
def createNamespace(String namespace, String registrySecret) {
  container ('kubectl') {
    ns_exists = sh(returnStatus: true, script: "kubectl get namespace ${namespace}")
    if (ns_exists != 0) {
      sh "kubectl create namespace ${namespace}"
      if (registrySecret) {
        giveRegistryAccessToNamespace (namespace, registrySecret)
      }
    }
  }
}

/*
  We have a (temporary) namespace that we want to grant ICP registry access to.
  String namespace: target namespace
  1. Port registrySecret into a temporary namespace
  2. Modify 'default' serviceaccount to use ported registrySecret.
*/

def giveRegistryAccessToNamespace (String namespace, String registrySecret) {
  sh "kubectl get secret ${registrySecret} -o json | sed 's/\"namespace\":.*\$/\"namespace\": \"${namespace}\",/g' | kubectl create -f -"
  sh "kubectl patch serviceaccount default -p '{\"imagePullSecrets\": [{\"name\": \"${registrySecret}\"}]}' --namespace ${namespace}"
}



